using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FnF.Models
{
    public class AppUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public DateTime JoiningDate { get; set; }
        public DateTime LastModified { get; set; }
        public string ImagePath { get; set; }
    }
}
