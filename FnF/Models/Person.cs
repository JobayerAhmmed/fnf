using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FnF.Models
{
    public class Person
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public DateTime AddingDate { get; set; }
        public string ImagePath { get; set; }

        public string UserId { get; set; }
        public AppUser User { get; set; }
    }
}
